#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}git-update-fork.sh${CRESET}
# Update a forked repo from original project
# Usage: ${COLOR_SCRIPT}git-update-fork.sh${CRESET}
# Aliases: git-update-fork
# Ex: git-update-fork
#     git-update-fork.sh
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
git fetch upstream
git checkout master
git merge upstream/master