#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}open.sh${CRESET}
# Script to open file or directory on explorer.
# Usage: ${COLOR_SCRIPT}open.sh${CRESET} [file | direcotry]
# Aliases: o, open
# Ex: open            -> open current location on explorer
#     open file.txt   -> open file on windows default text editor
#     o /opt/devtools -> open directory devtools on windows explorer
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
function _mainOpen() {
	local BASEDIR=$(dirname $0)
	local LOCALCOMMAND="explorer.exe"

	if [ "$1" = "" ] ; then
		exec $LOCALCOMMAND .
	else
		if [[ -d $1 ]]; then
			cd $1
			exec $LOCALCOMMAND .
			cd $BASEDIR
		elif [[ -f $1 ]]; then
		 	local REALFILE=$(readlink -f $1)
		 	cd $(dirname $REALFILE)
		 	exec $LOCALCOMMAND "$(cygpath -m ${REALFILE##*/})"
		else
			exec $LOCALCOMMAND $1
		fi;
	fi;
}
_mainOpen $@;
unset _mainOpen;