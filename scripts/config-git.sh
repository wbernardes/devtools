#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}config-git.sh${CRESET}
# Configurate .gitconfig base on /opt/devtools/config/git/.gitconfig.template
# Usage: ${COLOR_SCRIPT}config-git.sh${CRESET}
# Aliases: config-git
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh

mainSetupGitconfig () {
  if ! [ -f $HOME/.gitconfig ]; then
    infoh "config-git:"
    info 'setup gitconfig'

    git_credential='cache'
    git_default_editor='vim'

    user ' - What is your git author name?'
    read -e git_authorname
    user ' - What is your git author email?'
    read -e git_authoremail

    sed -e "s/AUTHORNAME/$git_authorname/g" -e "s/AUTHOREMAIL/$git_authoremail/g" -e "s/GIT_CREDENTIAL_HELPER/$git_credential/g" -e "s/GIT_DEFAULT_EDITOR/$git_default_editor/g" /opt/devtools/config/git/.gitconfig.template > ${HOME}/.gitconfig

	#----------------------------------------------------------------------------------------------------------
	# INSTALLING AN GIT KEYCHAIN FOR WINDOWS
	#----------------------------------------------------------------------------------------------------------
	if [ -n $(which git-credential-winstore.exe) ]; then
		git-credential-winstore.exe -s
		sleep 2s;
		git config --global credential.helper '!'"'/c/Users/${USER}/AppData/Roaming/GitCredStore/git-credential-winstore.exe'"
	fi

    successln 'gitconfig'
  fi

}

mainSetupGitconfig;
unset mainSetupGitconfig;
