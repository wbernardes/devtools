#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}patch-devtools.sh${CRESET}
# Patch devtools with DIFF file.
# Usage: ${COLOR_SCRIPT}pack-devtools.sh${CRESET} <diff_filename>
# Aliases: pack-devtools-7z
#
# Author: Luiz Rapatao
# Since: 2014-08-14
#===================================================================
source /opt/devtools/lib/utils.sh;

function doPatchDevtools7z() {
	if [[ ! -z ${1} ]]; then
		7z x $(cygpath -m ${1}) -aoa -y -o$(cygpath -m /);
	else
		docScript ${BASH_SOURCE[0]};
	fi;
}

doPatchDevtools7z ${1};
unset doPatchDevtools7z;