#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}pack.sh${CRESET}
# PIMS version package handler
# Usage: ${COLOR_SCRIPT}pack${CRESET} [version]
# Aliases:
#
# Author: Luiz Rapatao
# Since: 2015-10-14
#===================================================================
source /opt/devtools/lib/utils.sh;

PACK_FOLDER="${PROJECTS_HOME}/packages/";
PACK_SVN="/continuous_integration/trunk/packages_svn_externals/";

function _configure() {
    if [[ ! -d "${PACK_FOLDER}" ]]; then
        mkdir -p "${PACK_FOLDER}";
    fi;
}

function _isLocalPackageFolder() {
    local actual=$(pwd);
    if [[ "${actual:0:${#PACK_FOLDER}}" == "${PACK_FOLDER}" ]]; then
        echo 1;
    else
        echo 0;
    fi
}

function _identifyVersion() {
    if [[ "${1}" != "" ]]; then
        echo "${1}";
    else
        if [[ "$(_isLocalPackageFolder)" == 1 ]]; then
            local pack=$(echo $(pwd) | sed 's/.*packages\///g' | sed 's/\/.*//g');
            echo ${pack};
        fi;
    fi;
}

function _packUpdateAll() {
    find ${PACK_FOLDER}[^.]* -prune -type d | sed 's/.*\///g' | while read pack; do
        _packUpdate "${pack}";
    done;
}

function _packUpdate() {
    local pack=$(_identifyVersion ${1});

    if [[ "${pack}" != "" ]]; then
        infoh "Updating Project Package ${pack}";
        local folder="${PACK_FOLDER}${pack}";
        svnCheckout.sh "Package ${pack}" "${PACK_SVN}${pack}" "${folder}";
    else
        _packHelp;
    fi;
}

function _packHelp() {
    infoh "Type 'pack [version]' to checkout or update a package";
    infoh "If current path is in a package, you can omit version parameter!";
    infoh "To update all downloaded packages, use: ${COLOR_SCRIPT}pack all${CRESET}"
}

function main() {
    _configure;

    if [[ "${1}" == "all" ]]; then
        _packUpdateAll;
    else
        _packUpdate "${1}";
        if [[ "${1}" != "" ]]; then
            cd "${PACK_FOLDER}${1}";
        fi
    fi;

}

main $@;