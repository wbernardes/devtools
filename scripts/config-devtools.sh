#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}config-devtools.sh${CRESET}
# Setup devtools
#       - link main applications folders to /usr/local/opt,
#       - use SETX to export permanently some variables to windows,
#       - add current domain user to cygwin users and group
#       - call the following scripts:
#             * /opt/devtools/scripts/config-maven.sh
#             * /opt/devtools/scripts/config-gradle.sh
#             * /opt/devtools/scripts/config-dotfiles.sh
# Usage: ${COLOR_SCRIPT}config-devtools.sh${CRESET}
# Aliases: config-devtools
#
# Author: Diego Schmidt
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh

function _mainConfigDevtools() {
	mount --change-cygdrive-prefix /
	export TERM=cygwin
	infoh "config-devtools:"
	source /opt/devtools/dotfiles/.bash_profile > /dev/null
	_setxVariables;
	_addDomainUserToCygwinUsersGroup;
	config-git.sh
	_createSymbolicLinks;
	source /opt/devtools/scripts/config-dotfiles.sh
	export TERM=linux
	
	if [ ! -d "${HOME}/projects" ]; then
  		mkdir "${HOME}/projects" >> /dev/null
	fi
	
	if [ ! -d "${HOME}/documents" ]; then
  		mkdir "${HOME}/documents" >> /dev/null
	fi
	
	if [ ! -d "${HOME}/downloads" ]; then
  		mkdir "${HOME}/downloads" >> /dev/null
	fi
	
	if [ ! -d "${HOME}/sandbox" ]; then
  		mkdir "${HOME}/sandbox" >> /dev/null
	fi

}

function _createSymbolicLinks() {
	#----------------------------------------------------------------------------------------------------------
	# LINKING DEFAULT APPS
	#----------------------------------------------------------------------------------------------------------
	echo "";
	infoh "Linking Apps"
	#SSH
	if [[ -f ${HOME}/.ssh ]]; then
		rm -rf ${HOME}/.ssh;
	fi;
	mkdir -p ${HOME}/.ssh

	rm -rf ${HOME}/.ssh/config
	cp /opt/devtools/config/.ssh/config ${HOME}/.ssh/config
	chmod 600 ${HOME}/.ssh/config
	
	successln "Visual Studio"
	successln "Visual Code"
	successln "MsBuild"
	successln "7zip"
	successln "Baregrep"
	successln "Baretail"
	successln "Chrome"
	successln "Cntlm"
	successln "ConEmu"
	successln "DiffMerge"
	successln "Docker"
	successln "Evernote"
	successln "Fiddler"
	successln "Foobar2000"
	successln "Gcc-Tools"
	successln "Git"
	successln "Ilmerge"
	successln "Java"
	successln "JMeter"
	successln "JsonEdit"
	successln "Notepad++"
	successln "nUnit"
	successln "Pidgin"
	successln "ProcessExplorer"
	successln "RStudio"
	successln "SourceTree"
	successln "SqlDeveloper"
	successln "Thunderbird"
	successln "Vagrant"
	successln "VirtualBox"
	successln "VirtualWin"
	successln "Winamp"
	successln "XMind"
	successln "XmlExplorer"
	successln "Windows commands"
}

function _setxVariables() {
	#----------------------------------------------------------------------------------------------------------
	# PREPARING JAVA_OPTIONS VARIABLE
	#----------------------------------------------------------------------------------------------------------
	HOME_DIR="$(cygpath -m ${HOME})"

	#----------------------------------------------------------------------------------------------------------
	# EXPORTING PERMANENTLY
	#----------------------------------------------------------------------------------------------------------
	info "Exporting Variables Permanently (setx)"

	setx DEVTOOLS_HOME "$(cygpath -m ${DEVTOOLS_HOME})" > /dev/null
	setx DEVTOOLS_OPT "$(cygpath -m ${DEVTOOLS_OPT})" > /dev/null

	setx JAVA_HOME  "$(cygpath -m ${JAVA_HOME})" > /dev/null
	setx JAVA_OPTIONS "${JAVA_OPTIONS}" > /dev/null

	#setx GIT_HOME "$(cygpath -m ${GIT_HOME})" > /dev/null

	successln "Exporting Variables Permanently (setx)"
}

function _addDomainUserToCygwinUsersGroup() {
	#----------------------------------------------------------------------------------------------------------
	# ADDING DOMAIN USER TO CYGWIN USERS AND GROUP
	#----------------------------------------------------------------------------------------------------------
	info "Adding Domain User To Cygwin Users And Group"

	mkpasswd -l > /etc/passwd
	mkpasswd -c >> /etc/passwd
	mkgroup -l > /etc/group
	mkgroup -c >> /etc/group

	successln "Adding Domain User To Cygwin Users And Group"
}

function _creatingProjectsAndPull(){
		
		info "Clonning Projects"
		mkdir "${HOME}/projects" >> /dev/null
		cd "${HOME}/projects"
		echo " "
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/easynvest.loggerrepository.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/easynvest.framework.samples.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/easynvest.nugetserver.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/easynvest.framework.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/easynvest.auth.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/servicebi.git  > /dev/null
		git clone --recursive https://wbernardes@bitbucket.org/easynvest/qlikview.git  > /dev/null
		successln "Clonning Projects"
}

_mainConfigDevtools;
unset _mainConfigDevtools;
unset _createSymbolicLinks;
unset _setxVariables;
unset _addDomainUserToCygwinUsersGroup;


