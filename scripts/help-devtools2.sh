#!/bin/bash
##===================================================================
# ${COLOR_SCRIPT}help-devtools2.sh${CRESET}
# Devtools Help.
# Usage: ${COLOR_SCRIPT}help-devtools.sh${CRESET}
# Aliases: hd, help-devtools, devtools
#
# Author: Diego Schmidt
# Since: 2014-08-11
##===================================================================
source /opt/devtools/lib/utils.sh

function _mainHelpDevtools() {

	shopt -s globstar
	for file in /opt/devtools/scripts/*; do
		[ -r "$file" ] && [ -f "$file" ] && {
			local txt=$(awk '
				BEGIN {
					flag=0
				}
				/#===.*/{
					getline;
					if (flag==0)
						flag=1
					else {
						flag=0
						#print substr($0,2);
					}
				}
				flag { print substr($0,2);}

				' ${file} | head -2 | xargs -L2 echo )

			if [[ ! -z ${txt} ]]; then
				txt=$( eval echo -e ${txt} );
				local cmd=${txt%% *};
				local desc=${txt#* };
				printf "%-*s%s\n" 30 "${cmd}" "${desc}";
			fi;
		}
	done;

	unset file;
}

_mainHelpDevtools $@;
unset _mainHelpDevtools;