#!/bin/bash
#===================================================================
# ${COLOR_SCRIPT}pack-devtools.sh${CRESET}
# Pack devtools to distribute.
# Usage: ${COLOR_SCRIPT}pack-devtools.sh${CRESET} [-m | -s]
# Arguments: -m    pack min
#            -s    split 7z in 250m files
#            -h    help
# Aliases: pack-devtools
#
# Author: Luiz Rapatao
# Since: 2014-08-11
#===================================================================
source /opt/devtools/lib/utils.sh;


DEVTOOLS_PACK_DATE=$(date +%Y%m%d_%Hh%Mm%Ss);
DEVTOOLS_PACK_FOLDER="/dist";
DEVTOOLS_PACKNAME="DevTools-v6_x64";
DEVTOOLS_FULL_PATH_PACK_DIST="${DEVTOOLS_PACK_FOLDER}/${DEVTOOLS_PACKNAME}-PACK.7z";
DEVTOOLS_FULL_PATH_DIFF_DIST="${DEVTOOLS_PACK_FOLDER}/${DEVTOOLS_PACKNAME}-DIFF.7z";
DEVTOOLS_PACK_DATE_FILE="devtools-package-info";

DEVTOOLS_PACK_EXCLUSIONS_FILE="${DEVTOOLS_DATA}/pack_exclude_7z.txt";

function doPackageDevtools7z() {
	local SPLIT_ARG=;
	if [ "${1}" = "-h" ]; then
		docScript ${BASH_SOURCE[0]};
		return 1;
	fi

	[[ "${1}" = "-m" ]] || [[ "${2}" = "-m" ]] && {
		DEVTOOLS_PACK_EXCLUSIONS_FILE="${DEVTOOLS_DATA}/pack_exclude_7z.min.txt";
		DEVTOOLS_FULL_PATH_PACK_DIST="${DEVTOOLS_PACK_FOLDER}/${DEVTOOLS_PACKNAME}-${DEVTOOLS_PACK_DATE}-PACK.min.7z";
		DEVTOOLS_FULL_PATH_DIFF_DIST="${DEVTOOLS_PACK_FOLDER}/${DEVTOOLS_PACKNAME}-${DEVTOOLS_PACK_DATE}-DIFF.min.7z";
	}

	[[ "${1}" = "-s" ]] || [[ "${2}" = "-s" ]] && {
		SPLIT_ARG="-v250m";
	}

	mkdir ${DEVTOOLS_PACK_FOLDER} > /dev/null 2> /dev/null;

	echo ${DEVTOOLS_PACK_DATE} > /${DEVTOOLS_PACK_DATE_FILE};

	if [[ -f "${DEVTOOLS_FULL_PATH_PACK_DIST}" ]]; then

		local dir_path=$(cygpath -m ${DEVTOOLS_FULL_PATH_PACK_DIST});

		local LAST_PACK_DATE=$(7z l ${dir_path} 2> /dev/null | \
			grep -e 'devtools-package-info' | tr -d '[\-:]' | cut -d ' ' -f 1,2 | tr -d ' ');

		7z u $(cygpath -m ${DEVTOOLS_FULL_PATH_PACK_DIST}) $(cygpath -m /)  \
			-x@$(cygpath -m ${DEVTOOLS_PACK_EXCLUSIONS_FILE}) -xr!*.log -xr!'$USER_HOME' \
			-ms=off -up0q3x2z0!$(cygpath -m ${DEVTOOLS_FULL_PATH_DIFF_DIST});

	else
		echo "${SPLIT_ARG}"
		7z a $(cygpath -m ${DEVTOOLS_FULL_PATH_PACK_DIST}) $(cygpath -m /) \
			-x@$(cygpath -m ${DEVTOOLS_PACK_EXCLUSIONS_FILE}) -xr!*.log -xr!'$USER_HOME' -ms=off ${SPLIT_ARG};

	fi;
}

doPackageDevtools7z $@;
unset doPackageDevtools7z;