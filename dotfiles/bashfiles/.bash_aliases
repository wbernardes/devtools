#----------------------------------------------------------------------------------------------------------
# Removing sh from scripts
#----------------------------------------------------------------------------------------------------------
for file in /opt/devtools/scripts/**; do
	name=${file##*/}
	name=${name%.*}
	[ -r "$file" ] && [ -f "$file" ] && alias "${name}"="$file";
done;
unset file;
unset name;

#----------------------------------------------------------------------------------------------------------
# Default to human readable figures
#----------------------------------------------------------------------------------------------------------
alias df='df -h'
alias du='du -h'
alias du1='du -h --max-depth=1'
#
# Misc :)
alias less='less -r'                          # raw control characters
alias whence='type -a'                        # where, of a sort
alias grep='grep --color'                     # show differences in colour
alias egrep='egrep --color=auto'              # show differences in colour
alias fgrep='fgrep --color=auto'              # show differences in colour
#
# Some shortcuts for different directory listings
#LS_COLORS="di=1;34"
#export LS_COLORS

alias ls='ls -hF --color=tty --show-control-chars'  # classify files in colour
alias dir='ls --color=auto --format=vertical'
alias vdir='ls --color=auto --format=long'
alias ll='ls -larht'                              # long list
alias la='ls -A'                              # all but . and ..
alias l='ls -l'                              #
#
# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"

#----------------------------------------------------------------------------------------------------------
# Other aliases
#----------------------------------------------------------------------------------------------------------
alias cleartmp="rm -rf /tmp/*; rm -f /c/temp/*;"
alias cls='clear'
alias cyg='apt-cyg -m http://mirrors.kernel.org/sources.redhat.com/cygwin/'
alias g='git'

alias resource='source ~/.bash_profile'
alias reload="source /opt/devtools/dotfiles/.bash_profile"
alias start='cygstart'
alias vi="vim"
alias tree="tree -hf"

#----------------------------------------------------------------------------------------------------------
# Script aliases
#----------------------------------------------------------------------------------------------------------
alias TODO="todo"
alias cd=cd_func
alias o="open"
alias ua="source update-devtools.sh; echo; update-projects"
alias ud="source update-devtools.sh"
alias up="update-projects.sh"
alias upc="update-projects.sh --cleanup"
alias update-all="source update-devtools.sh; echo ; update-projects"
alias devtools="help-devtools2.sh"
alias db="dbconf.sh"
alias pack="source pack-util.sh"

#----------------------------------------------------------------------------------------------------------
# Places
#----------------------------------------------------------------------------------------------------------
alias op="cd ~/projects"
alias odev="cd /opt/devtools"
alias oh="cd ~"
alias opt="cd /opt"
alias tmp='cd /tmp'
alias documents='cd ~/documents'
alias downloads='cd ~/downloads'
alias projects='op'
alias sandbox='cd ~/sandbox'
alias home='cd ~'

#----------------------------------------------------------------------------------------------------------
# Java aliases
#----------------------------------------------------------------------------------------------------------
alias java8='export JAVA_HOME=/opt/java/jre1.8.0_66; export PATH=/opt/java/jre1.8.0_66/bin:$PATH';

#----------------------------------------------------------------------------------------------------------
# FuncoesZZ aliases
#----------------------------------------------------------------------------------------------------------
alias ora='zzora';
alias google='zzgoogle -n 50';
alias tv='zztv | grep -i';
alias cep='zzcep';
alias tempo='zztempo Brazil SBSP';
alias brasileirao='zzbrasileirao'
alias libertadores='zzlibertadores -c'
alias cotacao='zzcotacao'
alias futebol='zzfutebol'
alias porta='zzporta'
alias hoje='zzdata'
alias tradutor='zztradutor'
alias wiki='zzwikipedia'
alias cinemark='zzcinemark'
alias cinepolis='zzcinepolis'
alias uci='zzcineuci'
alias diasuteis='zzdiasuteis'
alias dicionario='zzdicportugues'
alias geoip='zzgeoip'
alias rastreamento='zzrastreamento'
alias oc='ochrome'
alias oci='ochromei'

#----------------------------------------------------------------------------------------------------------
# Git aliases
#----------------------------------------------------------------------------------------------------------
git config --global alias.l 'log --since=2.weeks --shortstat --pretty=format:"Commit: %h - %cn, %cr : %s"'



