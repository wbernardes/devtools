# base-files version 4.2-3

# ~/.bashrc: executed by bash(1) for interactive shells.

# The latest version as installed by the Cygwin Setup program can
# always be found at /etc/defaults/etc/skel/.bashrc

# Modifying /etc/skel/.bashrc directly will prevent
# setup from updating it.

# Ref: http://www.linuxjournal.com/content/globstar-new-bash-globbing-option
mount --change-cygdrive-prefix /
shopt -s globstar

source /opt/devtools/lib/utils.sh
source /opt/devtools/lib/window.sh;

# Load Files in the following order:
#   1 - ~/etc/etc/.bash*
for file in /opt/devtools/dotfiles/bashfiles/.bash*; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

#----------------------------------------------------------------
# STARTING X SERVER
#----------------------------------------------------------------
#startXWin;

#----------------------------------------------------------------
# SETTING PATH FOR CYGWIN AND POWERSHELL
#----------------------------------------------------------------
set PATH="$PATH:/c/Windows/System32/WindowsPowerShell/v1.0:/c/Windows:/c/Windows/System32:/opt/vagrant/bin"

echo "┌─┐┌─┐┌─┐┬ ┬┌┐┌┬  ┬┌─┐┌─┐┌┬┐\ \ \ \  ";
echo "├┤ ├─┤└─┐└┬┘│││└┐┌┘├┤ └─┐ │  ) ) ) ) ";
echo "└─┘┴ ┴└─┘ ┴ ┘└┘ └┘ └─┘└─┘ ┴ /_/ /_/  ";
echo "██████╗ ███████╗██╗   ██╗████████╗ ██████╗  ██████╗ ██╗     ███████╗";
echo "██╔══██╗██╔════╝██║   ██║╚══██╔══╝██╔═══██╗██╔═══██╗██║     ██╔════╝";
echo "██║  ██║█████╗  ██║   ██║   ██║   ██║   ██║██║   ██║██║     ███████╗";
echo "██║  ██║██╔══╝  ╚██╗ ██╔╝   ██║   ██║   ██║██║   ██║██║     ╚════██║";
echo "██████╔╝███████╗ ╚████╔╝    ██║   ╚██████╔╝╚██████╔╝███████╗███████║";
echo "╚═════╝ ╚══════╝  ╚═══╝     ╚═╝    ╚═════╝  ╚═════╝ ╚══════╝╚══════╝";
echo "                                                              1.0.1 ";