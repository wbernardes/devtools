#!/bin/bash
#===================================================================
# ${COLOR_LIB}window.sh${CRESET}
# Lib defining window functions
#
# Author: Luiz H. Rapatão
# Since: 2014-10-22
#===================================================================

function startXWin() {
    if [[ -f /usr/bin/xwin ]]; then
        local xwin=$(ps -Wa | grep -i xwin);
        if [[ ${xwin} == "" ]]; then
            cygstart xwin -multiwindow;
        fi;
        export DISPLAY=:0;
    fi;
}

function showDialog() {
    if [[ ${DISPLAY} != "" ]]; then
        xdialog --auto-placement "${@}";
    else
        dialog "${@}";
    fi
}
